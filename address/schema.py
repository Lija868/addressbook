from typing import Optional, Any

from pydantic import BaseModel
from pydantic.fields import ModelField
from pydantic.schema import datetime


class ResponseSchema(BaseModel):
    message: str
    total_count: Optional[int]
    data: Optional[Any]
    status_code: int


class AddressSchema(BaseModel):
    name: str
    street_name: str
    latitude: float
    longitude: float


class AddressListSchema(BaseModel):
    name: str
    street_name: str
    latitude: float
    longitude: float
    created_at: datetime
    modified_at: datetime


def to_schema(model, schema_class:BaseModel, many_to_many=False):
    if model:
        fields = schema_class.__fields__
        model_dict = {}
        for field_name in fields:
            field: ModelField = fields[field_name]
            if hasattr(model, field_name) or (
                    isinstance(model, dict) and field_name in model):
                if isinstance(model, dict):
                    value = model[field_name]
                else:
                    value = getattr(model, field_name)
                # finding the nested schema

                if hasattr(field.type_, "__fields__"):
                    is_many_to_many = hasattr(value, "all")
                    if is_many_to_many and many_to_many==True:
                        many_to_many_value = value.all()
                        model_dict[field_name]=[
                            to_schema(item, field.type_) for item in many_to_many_value]
                    elif hasattr(value, "__len__") and not hasattr(value, "keys"):
                        model_dict[field_name] = [
                            to_schema(item, field.type_) for item in value]
                    elif not is_many_to_many:
                        model_dict[field_name] = to_schema(value, field.type_)
                else:
                    property = getattr(schema_class, f"get_{field_name}", None)
                    if property:
                        model_dict[field_name]=property(schema_class, model)
                    else:
                        model_dict[field_name]=value
            else:
                property = getattr(schema_class, f"get_{field_name}", None)
                if property:
                    model_dict[field_name] = property(schema_class, model)
                else:
                    model_dict[field_name] = value
        return schema_class(**model_dict)
    return None



