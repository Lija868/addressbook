from django.contrib.gis.db.models.functions import GeometryDistance
from django.contrib.gis.geos import Point
from fastapi import APIRouter
from starlette import status

from address.models import Address
from address.schema import ResponseSchema, AddressSchema, to_schema, AddressListSchema

router = APIRouter(tags=["address"])


@router.post("/address", response_model=ResponseSchema)
def create_address(data: AddressSchema):
    """
    Api for creating the address
    Args:
        data : Address Schema
    Returns:
        Address object
    """
    try:
        record = data.__dict__
        record["location"] = Point(record.get("latitude"), record.get("longitude"), srid=4326)
        Address.objects.create(**record)
        return ResponseSchema(message="Address added successfully",
                              status_code=status.HTTP_200_OK)
    except Exception as e:
        return ResponseSchema(message=f"Something went wrong due to {str(e)}",
                              status_code=status.HTTP_400_BAD_REQUEST)


@router.get("/address", response_model=ResponseSchema)
def list_address(lat_min:float,long_max:float, lat_max:float=0, long_min:float=0,
                 ):
    """
    Api for listing the address
    Args:
        lat min and max cords
        long min and max cords
        distance

    Returns:
        Address object
    """
    try:
        queryset = Address.objects.all()
        if lat_max:
            queryset = queryset.filter(latitude__lte=lat_max)
        if lat_min:
            queryset = queryset.filter(latitude__gte=lat_min)
        if long_max:
            queryset = queryset.filter(longitude__lte=lat_max)
        if long_min:
            queryset = queryset.filter(longitude__gte=lat_min)
        # if distance:
        #     queryset = queryset.filter(location__dwithin=distance)

        results = [to_schema(query, AddressListSchema) for query in queryset]
        return ResponseSchema(message="Success",
                              data=results,
                              total_count = queryset.count(),
                              status_code=status.HTTP_200_OK)
    except Exception as e:
        return ResponseSchema(message=f"Something went wrong due to {str(e)}",
                              status_code=status.HTTP_400_BAD_REQUEST)



