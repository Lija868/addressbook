from django.contrib.gis.db.models import PointField
from django.contrib.gis.geos import Point
from django.db import models
from django.utils import timezone


class Address(models.Model):
    """The address model"""
    name = models.CharField(max_length=100)
    street_name = models.CharField(max_length=200)
    latitude = models.DecimalField(max_digits=13, decimal_places=10, null=True, blank=True)
    longitude = models.DecimalField(max_digits=13, decimal_places=10, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)
    modified_at = models.DateTimeField(auto_now=True)



