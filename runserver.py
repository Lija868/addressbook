import uvicorn

if __name__ == "__main__":
    uvicorn.run("all:app", host="localhost", port=8020,reload=True)
