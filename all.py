import os
import traceback

from fastapi import FastAPI, APIRouter
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.trustedhost import TrustedHostMiddleware

from addressbook.asgi import application  # this import is needed in order to load django apps

from addressbook import settings

from address import api as address_api

def context_path(url):
    return f"/api{url}"

app = FastAPI(title="Address Book API's",
              version="1.0.0",
              openapi_url=context_path("openapi.json"),
              docs_url=context_path("/docs")
              )
app.add_middleware(TrustedHostMiddleware, allowed_hosts=settings.ALLOWED_HOSTS)
app.add_middleware(CORSMiddleware,
                   allow_origins=settings.ALLOWED_ORIGINS,
                   allow_credentials=True,
                   allow_methods=["*"],
                   allow_headers=["*"]
                   )
router = APIRouter(
    tags=["Health Check"],
    responses={}
)

@router.get("/healthcheck")
def health_check():
    return "SUCCESS"

app.include_router(router, prefix="")
app.include_router(address_api.router, prefix="/api/v1")