Prerequisite
----------------
Ubuntu 18.04
Python 3.X
pip
virtualenv

Deployment
-------------

1. Install Python dependecies and virtual-env

sudo apt install python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools

sudo apt-get install virtualenv

2. Clone the project

git clone <repo> addressbook

3.Create Virtual Environment

cd addressbook/

 virtualenv -p $(which python3) venv

4.Activate Virtual ENV

source venv/bin/activate

5. Install dependencies

pip install -r requirements.txt

6. Run the server
    python manage.py migrate
    python runserver.py
7. API Swagger docs
 Navigate to  http://localhost:8020/docs